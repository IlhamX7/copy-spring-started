package com.alesha.starterkit.controllers.manajementVA;

import com.alesha.starterkit.models.NumberVA;
import com.alesha.starterkit.repositories.NumberRepository;
import com.alesha.starterkit.repositories.UserRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/management")
public class NumberController {
    @Autowired
    NumberRepository numberRepository;

    @Autowired
    UserRepository userRepository;

    @PostMapping(value = "/createNumber", consumes = "application/json", produces = "application/json")
    public ResponseEntity<NumberVA> createNumber(@RequestBody NumberVA number) {
        try {
            NumberVA numberVA = numberRepository
                    .save(new NumberVA(number.getId(), number.getTransaksi(), number.getJenis(), number.isStatus(), number.getLimits(),
                            number.isMode(), false));
            return new ResponseEntity<>(numberVA, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("iniError " + e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/updateNumber", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Optional<NumberVA>> updateNumber(@RequestBody(required = false) String body) {
        try {
            JSONObject reqBody = new JSONObject(body);
            Optional<NumberVA> numberId = numberRepository.findById(reqBody.getLong("id"));
            if (numberId.isPresent()) {
                NumberVA number_va = new NumberVA();
                number_va.setId(numberId.get().getId());
                number_va.setTransaksi(reqBody.getLong("transaksi"));
                number_va.setJenis(reqBody.getString("jenis"));
                number_va.setStatus(reqBody.getBoolean("status"));
                number_va.setLimits(reqBody.getLong("limits"));
                number_va.setMode(reqBody.getBoolean("mode"));
                number_va.setDeleted(reqBody.getBoolean("deleted"));
                numberRepository.save(number_va);
                return new ResponseEntity<>(numberId, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("iniError " + e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/getAllNumber", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> getAllNumber(@RequestBody(required = false) String body) {
        try {
            List<NumberVA> numberVa = numberRepository.findAll();
            return new ResponseEntity<>(numberVa, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("iniError " + e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/getNumberId", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Optional<NumberVA>> findById(@RequestBody(required = false) String body) {
        try {
            JSONObject requestBody = new JSONObject(body);
            Optional<NumberVA> numberId = numberRepository.findById(requestBody.getLong("id"));
            if (numberId.isPresent()) {
                return new ResponseEntity<>(numberId, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("iniError " + e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
