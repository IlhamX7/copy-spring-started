package com.alesha.starterkit.controllers.web;

import com.alesha.starterkit.models.Breadcrumb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class DashboardController {
    @Autowired
    private Environment env;

    @GetMapping("/dashboard")
    public String home(Model model) {
        List<Breadcrumb> breadcrumbList = new ArrayList<>();

        Breadcrumb home = new Breadcrumb();
        Breadcrumb dashboard = new Breadcrumb();

        home.setLabel("Home");
        home.setLink("/");
        home.setActive(false);

        dashboard.setLabel("Dashboard");
        dashboard.setActive(true);

        breadcrumbList.add(home);
        breadcrumbList.add(dashboard);

        model.addAttribute("breadcrumbs",breadcrumbList);
        model.addAttribute("page_title","My Dashboard");
        model.addAttribute("app_name",env.getProperty("alesha.app.name"));

        return "dashboards/dashboard";
    }
}
