package com.alesha.starterkit.controllers.web;

import com.alesha.starterkit.models.Tutorial;
import com.alesha.starterkit.repositories.TutorialCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CrudController {
    @Autowired
    TutorialCrudRepository tutorialCrudRepository;
    private final int ROW_PER_PAGE = 5;

    @GetMapping("/crud")
    public String showTutorialList(Model model, @RequestParam(value = "page", defaultValue = "1") int pageNumber) {
        List<Tutorial> tutorials = new ArrayList<>();
        Pageable sortedByIdAsc = PageRequest.of(pageNumber - 1, ROW_PER_PAGE,
                Sort.by("id").ascending());
        tutorialCrudRepository.findAll(sortedByIdAsc).forEach(tutorials::add);
        long count = tutorialCrudRepository.count();
        boolean hasPrev = pageNumber > 1;
        boolean hasNext = (pageNumber * ROW_PER_PAGE) < count;

        model.addAttribute("hasPrev", hasPrev);
        model.addAttribute("prev", pageNumber - 1);
        model.addAttribute("hasNext", hasNext);
        model.addAttribute("next", pageNumber + 1);
        model.addAttribute("tutorials", tutorials);

        return "tutorial-index";
    }

    @GetMapping("/crud/add")
    public String addUser(Tutorial tutorial) {
        return "tutorial-add";
    }

    @PostMapping("/crud/save")
    public String addUser(@Valid Tutorial tutorial, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "tutorial-add";
        }

        tutorialCrudRepository.save(tutorial);
        return "redirect:/crud";
    }

    @GetMapping("/crud/edit/{id}")
    public String editTutorial(@PathVariable("id") long id, Model model) {
        Tutorial tutorial = tutorialCrudRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

        model.addAttribute("tutorial", tutorial);
        return "tutorial-edit";
    }

    @PostMapping("/crud/update/{id}")
    public String updateTutorial(@PathVariable("id") long id, @Valid Tutorial tutorial,
                                 BindingResult result, Model model) {
        if (result.hasErrors()) {
            tutorial.setId(id);
            return "update-user";
        }

        tutorialCrudRepository.save(tutorial);
        return "redirect:/crud";
    }

    @GetMapping("/crud/delete/{id}")
    public String deleteTutorial(@PathVariable("id") long id, Model model) {
        Tutorial tutorial = tutorialCrudRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid tutorial Id:" + id));
        tutorialCrudRepository.delete(tutorial);
        return "redirect:/crud";
    }
}
