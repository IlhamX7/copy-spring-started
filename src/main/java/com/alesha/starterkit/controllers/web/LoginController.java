package com.alesha.starterkit.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @Autowired
    private Environment env;

    @GetMapping("/login")
    public String login(Model model) {

        model.addAttribute("page_title","My Dashboard");
        model.addAttribute("app_name",env.getProperty("alesha.app.name"));

        return "auth/login";
    }

}
