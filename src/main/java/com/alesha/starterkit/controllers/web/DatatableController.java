package com.alesha.starterkit.controllers.web;

import com.alesha.starterkit.repositories.TutorialDatatableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DatatableController {
    @Autowired
    TutorialDatatableRepository tutorialDatatableRepository;

    @GetMapping("/datatable/example")
    public String index(Model model) {
        return "datatable-list";
    }
}
