package com.alesha.starterkit.controllers.rest;

import com.alesha.starterkit.models.Tutorial;
import com.alesha.starterkit.repositories.TutorialDatatableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/service/datatable")
public class DatatableTutorialController {
    @Autowired
    TutorialDatatableRepository tutorialDatatableRepository;

    @PostMapping("/tutorials")
    public DataTablesOutput<Tutorial> getTutorials(@Valid @RequestBody DataTablesInput input) {
        return tutorialDatatableRepository.findAll(input);
    }
}
