package com.alesha.starterkit.controllers.rest;

import com.alesha.starterkit.StarterKitApplication;
import com.alesha.starterkit.models.Tutorial;
import com.alesha.starterkit.services.TutorialServices;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class HelloController {
    @Autowired
    TutorialServices tutorialServices;
    private static final Logger logger = LogManager.getLogger(StarterKitApplication.class);

    @GetMapping("/")
    public Map<String, String> index() {

        HashMap<String, String> map = new HashMap<>();
        map.put("hello", "world");
        return map;

    }

    @GetMapping("/welcome")
    public ObjectNode welcome() {

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode res = objectMapper.createObjectNode();
        ArrayNode data = JsonNodeFactory.instance.arrayNode();
        ObjectNode item = objectMapper.createObjectNode();
        item.put("message", "welcome");
        data.add(item);
        res.put("status", true);
        res.put("data",data);
        return res;

    }

    @GetMapping("/ws_tutorial")
    public ResponseEntity<List<Tutorial>> getTutorials() {

        return new ResponseEntity<>(tutorialServices.findAll(), HttpStatus.OK);

    }

    @GetMapping("/ws_tutorial/{id}")
    public ResponseEntity<Tutorial> getTutorialById(@PathVariable("id") long id) {

        return tutorialServices.findById(id);

    }
}
