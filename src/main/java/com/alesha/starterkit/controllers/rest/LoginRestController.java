package com.alesha.starterkit.controllers.rest;

import com.alesha.starterkit.exceptions.ForbiddenException;
import com.alesha.starterkit.models.LoginDto;
import com.alesha.starterkit.models.LoginResponseDto;
import com.alesha.starterkit.services.ValidateCaptcha;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/auth")
public class LoginRestController {

    @Autowired
    ValidateCaptcha service;

    @PostMapping("/login")
    @ResponseStatus(code = HttpStatus.OK)
    public LoginResponseDto welcome(@RequestBody final LoginDto dto) {
        final boolean isValidCaptcha = service.validateCaptcha(dto.getCaptchaResponse());
        if (!isValidCaptcha) {
            log.info("Throwing forbidden exception as the captcha is invalid.");
            return new LoginResponseDto("Invalid Captcha");
        }

        return new LoginResponseDto("Greetings " + dto.getEmail());
    }
}
