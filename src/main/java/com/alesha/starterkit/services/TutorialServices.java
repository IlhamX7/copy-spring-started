package com.alesha.starterkit.services;

import com.alesha.starterkit.constants.ApiService;
import com.alesha.starterkit.models.Tutorial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class TutorialServices {
    @Autowired
    private WebClient webClient;

    public List<Tutorial> findAll()
    {
        return webClient.get()
                .uri(ApiService.API_HOST+"/api/tutorials")
                .retrieve()
                .bodyToFlux(Tutorial.class)
                .collect(Collectors.toList())
                .share()
                .block();
    }

    public ResponseEntity<Tutorial> findById(long id){

        return webClient.get()
                .uri(ApiService.API_HOST+"/api/tutorials/"+id)
                .retrieve()
                .toEntity(Tutorial.class)
                .block();

    }
}
