package com.alesha.starterkit.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginDto {

    @NonNull
    String email;
    @NonNull
    String password;
    @NonNull
    String captchaResponse;

}
