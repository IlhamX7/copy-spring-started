package com.alesha.starterkit.models;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "number_va")
public class NumberVA {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "transaksi")
    private Long transaksi;

    @Column(name = "jenis")
    private String jenis;

    @Column(name = "status")
    private boolean status;

    @Column(name = "limits")
    private Long limits;

    @Column(name = "mode")
    private boolean mode;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false, updatable = false)
    private Date created_at;

    @PrePersist
    private void onCreate() {
        created_at = new Date();
    }

    @Column(name = "created_by", nullable = true, updatable = false)
    private Long created_by;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "updated_at", nullable = true, updatable = true)
    private Date updated_at;

    @PreUpdate
    protected void onUpdate() {
        updated_at = new Date();
    }

    @Column(name = "updated_by", nullable = true, updatable = true)
    private Long updated_by;

    public NumberVA() {
    }

    public NumberVA(Long id, Long transaksi, String jenis, boolean status, Long limits, boolean mode, boolean deleted) {
        this.id = id;
        this.transaksi = transaksi;
        this.jenis = jenis;
        this.status = status;
        this.limits = limits;
        this.mode = mode;
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(Long transaksi) {
        this.transaksi = transaksi;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Long getLimits() {
        return limits;
    }

    public void setLimits(Long limits) {
        this.limits = limits;
    }

    public boolean isMode() {
        return mode;
    }

    public void setMode(boolean mode) {
        this.mode = mode;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Long getCreated_by() {
        return created_by;
    }

    public void setCreated_by(Long created_by) {
        this.created_by = created_by;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public Long getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(Long updated_by) {
        this.updated_by = updated_by;
    }
}
