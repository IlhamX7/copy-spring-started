package com.alesha.starterkit.repositories;

import com.alesha.starterkit.models.Tutorial;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TutorialCrudRepository extends PagingAndSortingRepository<Tutorial, Long>,
        JpaSpecificationExecutor<Tutorial> {

}