package com.alesha.starterkit.repositories;

import com.alesha.starterkit.models.Tutorial;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface TutorialDatatableRepository extends DataTablesRepository<Tutorial, Integer> {}
