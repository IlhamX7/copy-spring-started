package com.alesha.starterkit.repositories;

import com.alesha.starterkit.models.NumberVA;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface NumberRepository extends JpaRepository<NumberVA, Long> {
    Optional<NumberVA> findById(Long id);

    List<NumberVA> findByStatus(boolean status);
}
